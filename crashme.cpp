#include <stdexcept>
#include <set>
#include <string>
#include <iostream>
#include <vector>

class User {
public:
    std::string username;
    uint64_t visits;

    User(std::string username) {
        this->username = username;
    }
};

std::vector<User> my_logs = {User("admin")};

void NewUser() {
    std::cout << "Username to add: ";
    const int username_limit = 20;
    std::string line;
    while (std::cin && line.size() < username_limit) {
        int symbol = std::cin.get();
        if (symbol == '\n') {
            break;
        }
        line.push_back(symbol);
    }
    my_logs.push_back(User(line));
    std::cout << "Successfully added user;\n";
}

void NewVisit() {
    std::cout << "Username to visit: ";
    const int username_limit = 20;
    std::string line;
    while (std::cin && line.size() < username_limit) {
        int symbol = std::cin.get();
        if (symbol == '\n') {
            break;
        }
        line.push_back(symbol);
    }

    for (auto it = my_logs.begin(); it != my_logs.end(); it++) {
        if (it->username == line) {
            it->visits++;
        }
    }
    
    std::cout << "Successfully visited (if user existed);\n";
}

void DeleteUser() {
    std::cout << "Username to delete: ";
    const int username_limit = 20;
    std::string line;
    while (std::cin && line.size() < username_limit) {
        int symbol = std::cin.get();
        if (symbol == '\n') {
            break;
        }
        line.push_back(symbol);
    }
    
    auto finder = my_logs.end();
    
    for (auto it = my_logs.begin(); it != my_logs.end(); it++) {
        if (it->username == line) {
            finder = it;
            my_logs.erase(finder);
        }
    }

    std::cout << "Successfully deleted (if user existed);\n";
}

int main() {
    // Можно вводить только команды: NewUser(); / NewVisit(); / DeleteUser(), не более 100 комманд;
    std::cout << "All done!\n";
}
